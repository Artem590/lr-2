#include <iostream>
#include <fstream>
#include <vector>
#define _USE_MATH_DEFINES
#include <math.h>
#include <iomanip>

#include <omp.h>

#include "utils.h"
#include "greens_applications.h"

#undef sum
#include <Eigen/Dense>

//using namespace Eigen;
using namespace std;

struct point2 {
	double x, y;
};

struct point3 {
	double x, y, z;
};

template <typename T>
T C_norm(vector<T> f) {
	for_each(f.begin(), f.end(), [](T &elem) {elem = fabs(elem); });
	return *max_element(f.begin(), f.end());
}

template <typename T>
T L2_norm(vector<T> f, T h) {
	for_each(f.begin(), f.end(), [](T &elem) {elem = sqr(elem); });
	T S = 0;
	for (auto& elem : f)
		S += elem;
	return sqrt(S * h);
}

template <typename T>
vector<T> tabulate(function<T(T)> f, T a, T b, int n) {
	vector<T> res(n);
	T dx = (b - a) / n;

	for (int i = 0; i < n; ++i)
		res[i] = f(a + i * dx + dx / 2);

	return res;
}

// Z := a*X + b*Y
template <typename T>
vector<T> axpy(T a, vector<T> X, T b, vector<T> Y) {
	vector<T> Z(X.size());
	for (int i = 0; i < Z.size(); ++i)
		Z[i] = a * X[i] + b * Y[i];
	return Z;
}

// ||Au - f||, ��� A - ������������ �������� � ����� K
template <typename T>
T discrepancy(const task_params &params, vector<T> u, vector<T> f) {
	int n = params.n;
	T h = params.hy;
	T ts_start = params.y_start;
	vector<T> Au_f(n);

	for (int i = 0; i < n; ++i) {
		T integral = 0;
		double t = ts_start + i * h + h / 2;
		for (int j = 0; j < n; ++j) {
			double s = ts_start + j * h + h / 2;
			integral += h * K(params, t, s) * u[j];
		}
		Au_f[i] = integral - f[i];
	}

	return L2_norm(Au_f, h);
}

void fill_nonregular_part(Eigen::MatrixXd &A, const task_params &params,
	int n, double ht, double q, double p, double alpha) {

	for (int i = 0; i < n; ++i) {
		A(i, i) += alpha;

		if (i > 0 && i < n - 1) {
			A(i, i) += 2 * alpha * p / ht / ht;
			A(i, i - 1) -= alpha * p / ht / ht;
			A(i, i + 1) -= alpha * p / ht / ht;
		}
		else if (i == 0) {
			A(i, i) -= alpha * p / ht / ht;
			A(i, i + 1) += 2 * alpha * p / ht / ht;
			A(i, i + 2) -= alpha * p / ht / ht;
		}
		else {
			A(i, i) -= alpha * p / ht / ht;
			A(i, i - 1) += 2 * alpha * p / ht / ht;
			A(i, i - 2) -= alpha * p / ht / ht;
		}
	}
}

void fill_rhs(Eigen::VectorXd &B, const task_params &params, int n, double ht,
	function<double(double)> f) {

	double h = params.hy;
	double ts_start = params.y_start;

	for (int i = 1; i < n - 1; ++i) {
		double t = ts_start + i * ht + ht / 2;
		B(i) = f(t);
	}
}

template<typename T>
vector<T> eigen_to_std_vec(const Eigen::VectorXd &X) {
	vector<T> res((int)X.rows());
	std::copy(X.data(), X.data() + (int)X.rows(), res.data());
	return res;
}

int main(int argc, char *argv[]) {

	double a = 1. / sqrt(2);
	double b = 1.;
	double x_start = 0.001, x_end = a - 0.001;
	double y_start = 0.001, y_end = b - 0.001;

	double convergence_eps = 1.e-10; // 100 * numeric_limits<double>::epsilon();
	
	int n = 100;

	double alpha_start;
	double alpha_end;
	double p;
	double stohastic_eps;
	double dichotomy_eps = 1.e-8;
	bool bc_correction = false;
	bool l2plot_by_alpha = false;
	
	if (argc < 7) {
		cout << "Too few input parametrs. See README." << endl;
		exit(1);
	}

	for (int i = 1; i < argc; ++i) {
		if (strcmp(argv[i], "-n") == 0)
			n = stol(argv[i + 1]);
		if (strcmp(argv[i], "-start") == 0)
			alpha_start = stod(argv[i + 1]);
		if (strcmp(argv[i], "-end") == 0)
			alpha_end = stod(argv[i + 1]);
		if (strcmp(argv[i], "-p") == 0)
			p = stod(argv[i + 1]);
		if (strcmp(argv[i], "-eps") == 0)
			stohastic_eps = stod(argv[i + 1]);
		if (strcmp(argv[i], "-bc") == 0)
			bc_correction = true;
		if (strcmp(argv[i], "-l2plot") == 0)
			l2plot_by_alpha = true;
	}

	double dpsi20 = 0;
	double dpsi2b = 0;

	if (bc_correction) {
		dpsi20 = -(1. / 4)*M_PI*cos((1. / 4)*M_PI);
		dpsi2b = -(1. / 4)*M_PI;
	}

	double h_x = (x_end - x_start) / n;
	double h_y = (y_end - y_start) / n;
	double border_eps_y = h_y / 10;

	auto phi_1 = [](double x) {
		return x;
	};
	auto phi_2 = [](double x) {
		return 0;
	};
	auto psi_1 = [](double y) {
		return sin(M_PI * y);
	};
	auto psi_2 = [](double y) {
		return sin(M_PI * (1 - y) / 4);
	};
	auto f = [](double x, double y) {
		return x*y;
	};

	Eigen::MatrixXd A(n - 1, n - 1);
	Eigen::VectorXd B(n - 1);
	
	// !!!
	n--;

	task_params params(a, b, x_start, y_start, x_end, y_end, convergence_eps, stohastic_eps, n);
	params.phi_1 = phi_1;
	params.phi_2 = phi_2;
	params.psi_1 = psi_1;
	params.psi_2 = psi_2;
	params.f = f;

	double hs = (y_end - y_start) / (n - 1);
	double ht = (y_end - y_start) / (n - 1);
	double q = 1;

	// ���������� ���������� ����� �������
	A(0, 0) = -3;
	A(0, 1) = 4;
	A(0, 2) = -1;
	B(0) = 0;
	A(n - 1, n - 1) = -3;
	A(n - 1, n - 2) = 4;
	A(n - 1, n - 3) = -1;
	B(n - 1) = 0;
	for (int i = 1; i < n - 1; ++i) {
		double t = y_start + i * ht + ht / 2;
		A(i, i) = 0;

		for (int j = 0; j < n; ++j) {
			double s = y_start + j * hs + hs / 2;
			double ksym = Ksym(params, t, s);
			A(i, j) = ksym * hs;
		}
	}

	vector<double> ft_tabulated;
	vector<double> gy_tabulated;
	vector<double> c_tabulated;

	std::function<double(double)>  C = [params, dpsi20, dpsi2b](double s) {
		return (-dpsi20 * (1 - s) * (1 - s) / 2 + dpsi2b * s * s / 2);
	};
	c_tabulated = tabulate(C, y_start, y_end, n);

	std::function<double(double)> correction = [params, dpsi20, dpsi2b, C](double t) {
		int n = params.n;
		double y_start = params.y_start;
		double y_end = params.y_end;

		auto to_integrate = [params, dpsi20, dpsi2b, t, C](double s) {
			return Ksym(params, t, s) * C(s);
		};

		double integral;
		rectangle_integrate(y_start, y_end, to_integrate, n + 1, integral);
		return integral;
	};
	vector<double> correction_tabulated = tabulate(correction, 0., b, n);

	auto f_corrected = [params, dpsi20, dpsi2b, correction](double t) {
		return ft(params, t) - correction(t);
	};

	function<double(double)> to_tabulate = [dpsi20, dpsi2b, params](double y) {
		return hy(params, y);
	};
	ft_tabulated = tabulate(to_tabulate, y_start, y_end, n);

	to_tabulate = [psi_2](double y) {
		return psi_2(y);
	};
	gy_tabulated = tabulate(to_tabulate, y_start, y_end, n);

	double delta;
	delta = L2_norm(
		tabulate(
			std::function<double(double)>(
				[params](double y) {return hy(params, y) - hy_precise(params, y); } 
			), y_start, y_end, n
		), h_y
	);

	ofstream hyplot("hy");
	for (int j = 0; j < n - 1; ++j) {
		double y = y_start + j * h_y;
		hyplot << y << '\t' << hy(params, y) << endl;
	}
	hyplot.close();

	// ��������� �� alpha
	double uleft_norm, uright_norm;

	//����� ��� ���������
	vector<pair<function<double(vector<double>)>, string>> norms = {
		{ [h_y, params, ft_tabulated, delta](vector<double> u) 
			{ 
				return fabs(discrepancy(params, u, ft_tabulated) - delta); 
			},
			"L2 norm discrepancy"
		},
		{ [gy_tabulated, ht](vector<double> f) {
			return L2_norm(axpy(1., f, -1., gy_tabulated), ht);
		}, "L2 norm"},
		{ [gy_tabulated](vector<double> f) {
			return C_norm(axpy(1., f, -1., gy_tabulated));
		}, "C norm"},
	};

	ofstream alphas("alphas.dat");

	fill_rhs(B, params, n, ht, f_corrected);

	for (auto nrm : norms) {

		double left = alpha_start;
		double right = alpha_end;
		double centr, centr_r, centr_l;
		vector<double> uleft, uright;

		do {
			centr = left + (right - left) / 2;
			centr_r = centr + (right - left) / 4;
			centr_l = centr - (right - left) / 4;

			Eigen::MatrixXd lsh_left = A;
			Eigen::MatrixXd lsh_right = A;
			fill_nonregular_part(lsh_left, params, n, ht, q, p, centr_l);
			fill_nonregular_part(lsh_right, params, n, ht, q, p, centr_r);

			Eigen::VectorXd x = lsh_left.fullPivLu().solve(B);
			uleft = axpy(1., eigen_to_std_vec<double>(x), 1., c_tabulated);
			x = lsh_right.fullPivLu().solve(B);
			uright = axpy(1., eigen_to_std_vec<double>(x), 1., c_tabulated);

			uleft_norm = nrm.first(uleft);
			uright_norm = nrm.first(uright);

			if (uleft_norm < uright_norm)
				right = centr;
			else
				left = centr;

		} while (fabs(uleft_norm - uright_norm) > dichotomy_eps);

		ofstream fout(nrm.second);
		for (int i = 0; i < n; ++i)
			fout << 0 + i * ht << '\t' << uleft[i] << endl;
		fout.close();

		cout << nrm.second << '\t' << norms[1].first(uleft) << '\t' << "with alpha\t" << centr << endl;
		alphas << setprecision(3) << centr << endl;
	}

	cout << "delta" << '\t' << delta << endl;
	alphas.close();

	if (l2plot_by_alpha) {
		ofstream l2plot("l2 by alpha");

		double da0 = 1.e-12;

		for (double alpha = alpha_start; alpha < alpha_end; alpha += da0*=1.5) {
			Eigen::MatrixXd lsh = A;
			fill_nonregular_part(lsh, params, n, ht, q, p, alpha);

			Eigen::VectorXd x = lsh.fullPivLu().solve(B);
			auto u = axpy(1., eigen_to_std_vec<double>(x), 1., c_tabulated);

			l2plot << alpha << '\t' << norms[0].first(u) << endl;
		}

		l2plot.close();
	}

	/*vector<point2> gy_array(n);
	ofstream gplot("g");
#pragma omp parallel for schedule(dynamic, 25) shared(gy_array)
	for (int j = 0; j < n; ++j) {
		double y = y_start + j * hy;
		gy_array[j] = { y, gy(params, y) };
	}

	for (int j = 0; j < n; ++j)
		gplot << gy_array[j].x << '\t' << gy_array[j].y << endl;
	gplot.close();
	
	ofstream Uplot("U");
	vector<point3> u_array(n * n);
	int p_prev = 0;
	int progress = 0;
	int num_threads = omp_get_max_threads();
#pragma omp parallel for schedule(guided, 10) firstprivate(progress, p_prev)
	for (int i = 0; i < n; ++i) {
		double x = x_start + i * hx;
		for (int j = 0; j < n; ++j) {
			double y = y_start + j * hy;
			u_array[i * n + j] = { x, y, u(params, x, y) };
		}

		if (omp_get_thread_num() == 0) 
			if (num_threads * 100 * progress / n > p_prev) {
				p_prev = num_threads * 100 * progress / n;
				cout << p_prev << "%" << endl;
			}

		progress++;
	}

	for (int i = 0; i < n; ++i)
		for (int j = 0; j < n; ++j)
			Uplot << u_array[i * n + j].x << '\t' << u_array[i * n + j].y << '\t' << u_array[i * n + j].z << endl;
	Uplot.close();
*/

	return 0;
}