#include "greens_applications.h"
#include "utils.h"

#include <stdint.h>
#define _USE_MATH_DEFINES
#include <math.h>

using namespace std;

task_params::task_params(double a, double b, double x_start, double y_start,
	double x_end, double y_end, double eps, double stohastic_eps, int n) {

	this->a = a;
	this->b = b;
	this->x_start = x_start;
	this->y_start = y_start;
	this->x_end = x_end;
	this->y_end = y_end;
	this->eps = eps;
	this->stohastic_eps = stohastic_eps;
	this->n = n;

	// !!!
	hx = (x_end - x_start) / n;
	hy = (y_end - y_start) / n;

	border_eps_y = hy / 10;

	distribution = uniform_real_distribution<double>(0., 1.);
	theta.resize(n);
	for (auto &ti : theta)
		ti = distribution(generator);
}


double Hn(double a, double b, double eta, double y, uint64_t n) {
	double pn = M_PI * n / a;
	if (eta <= y)
		return 0.5 * exp(pn * (eta - y)) * (1 - exp(-2 * pn*eta)) *
		(1 - exp(-2 * pn*(b - y))) / (1 - exp(-2 * pn*b));
	else
		return 0.5 * exp(pn * (y - eta)) * (1 - exp(-2 * pn*y)) *
		(1 - exp(-2 * pn*(b - eta))) / (1 - exp(-2 * pn*b));

}

double Hn_diff_eta(double a, double b, double eta0, double y, uint64_t n) {
	double pn = M_PI * n / a;
	if (eta0 <= y)
		return 0.5 * pn * exp(pn * (eta0 - y)) * (1 + exp(-2 * pn*eta0)) *
		(1 - exp(-2 * pn*(b - y))) / (1 - exp(-2 * pn*b));
	else
		return 0.5 * pn * exp(pn * (y - eta0)) * (1 - exp(-2 * pn*y)) *
		(1 + exp(-2 * pn*(b - eta0))) / (1 - exp(-2 * pn*b));

}

double G(double a, double b, double eps, double x, double y, double ksi, double eta) {
	double sum = 0;
	double prev, next = 0, pn;
	uint64_t n = 1;

	do {
		prev = next;
		double pn = M_PI * n / a;
		double _Hn;

		if (fabs(y - eta) < 10 * eps) {
			_Hn = Hn(a, b, eta + 5 * eps, y, n);
			_Hn = (_Hn + Hn(a, b, eta - 5 * eps, y, n)) / 2;
		}
		else
			_Hn = Hn(a, b, eta, y, n);

		next = sin(pn*x) * sin(pn*ksi) * _Hn / (pn);
		sum += next;
		n++;
	} while (fabs(prev - next) > eps && n < numeric_limits<uint64_t>::max());

	return (double)(2 * sum / a);
}

double G_diff_x(double a, double b, double eps, double x0, double y, double ksi, double eta) {
	double sum = 0;
	double prev, next = 0, pn;
	uint64_t n = 1;

	do {
		prev = next;
		double pn = M_PI * n / a;
		double _Hn;

		if (fabs(y - eta) < 10 * eps) {
			_Hn = Hn(a, b, eta + 5 * eps, y, n);
			_Hn = (_Hn + Hn(a, b, eta - 5 * eps, y, n)) / 2;
		}
		else
			_Hn = Hn(a, b, eta, y, n);

		next = pn * cos(pn * x0) * sin(pn*ksi) * _Hn / (pn);
		sum += next;
		n++;
	} while (fabs(prev - next) > eps && n < numeric_limits<uint64_t>::max());

	return (double)(2 * sum / a);
}

double G_diff_eta(double a, double b, double eps, double x, double y, double ksi, double eta0) {
	double sum = 0;
	double prev, next = 0, pn;
	uint64_t n = 1;

	do {
		prev = next;
		double pn = M_PI * n / a;
		next = sin(pn*x) * sin(pn*ksi) * Hn_diff_eta(a, b, eta0, y, n) / (pn);
		sum += next;
		n++;
	} while (fabs(prev - next) > eps && n < numeric_limits<uint64_t>::max());

	return (double)(2 * sum / a);
};

double diff_x_G_diff_eta(double a, double b, double eps, double x0, double y, double ksi, double eta0) {
	double sum = 0;
	double prev, next = 0, pn;
	uint64_t n = 1;

	do {
		prev = next;
		double pn = M_PI * n / a;
		next = cos(pn * x0) * sin(pn*ksi) * Hn_diff_eta(a, b, eta0, y, n);
		sum += next;
		n++;
	} while (fabs(prev - next) > eps && n < numeric_limits<uint64_t>::max());

	return (double)(2 * sum / a);
};


double G_diff_ksi(double a, double b, double eps, double x, double y, double ksi, double eta) {
	double sum = 0;
	double prev, next = 0, pn;
	uint64_t n = 1;

	do {
		prev = next;
		double pn = M_PI * n / a;
		double _Hn;

		if (fabs(y - eta) < 10 * eps) {
			_Hn = Hn(a, b, eta + 5 * eps, y, n);
			_Hn = (_Hn + Hn(a, b, eta - 5 * eps, y, n)) / 2;
		}
		else
			_Hn = Hn(a, b, eta, y, n);
		next = sin(pn*x) * cos(pn*ksi) * _Hn;
		sum += next;
		n++;
	} while (fabs(prev - next) > eps && n < numeric_limits<uint64_t>::max());

	return (double)(2 * sum / a);
};

double diff_x_G_diff_ksi(double a, double b, double eps, double y, double x0, double ksi0, double eta) {
	double sum = 0;
	double prev, next = 0, pn;
	uint64_t n = 1;

	do {
		prev = next;
		double pn = M_PI * n / a;
		double _Hn;

		if (fabs(y - eta) < 10 * eps) {
			_Hn = Hn(a, b, eta + 5 * eps, y, n);
			_Hn = (_Hn + Hn(a, b, eta - 5 * eps, y, n)) / 2;
		}
		else
			_Hn = Hn(a, b, eta, y, n);
		next = pn * cos(pn * x0) * cos(pn * ksi0) * _Hn;
		sum += next;
		n++;
	} while (fabs(prev - next) > eps && n < numeric_limits<uint64_t>::max());

	return (double)(2 * sum / a);
};

double K(const task_params &params, double y, double eta) {
	double a = params.a;
	double b = params.b;
	double eps = params.eps;

	auto An = [y, eta, a, b](uint64_t n) {
		double qm = M_PI * n / b;
		return -qm * sin(qm * y) * sin(qm * eta) / sinh(qm * a);
	};
	double res;
	sum(An, res, eps);
	return 2 * res / b;
}

double Int_phi_1(const task_params &params, double x, double y) {
	double a = params.a;
	double b = params.b;
	double eps = params.eps;

#ifndef APPROX
	auto An = [x, y, b, a](uint64_t n) {
		double pn = M_PI * n / a;
		return sin(pn * x) * (sin(pn * a) - pn * a * cos(pn * a)) *
			exp(-pn * y) * (1 - exp(-2 * pn * (b - y))) / (1 - exp(-2 * pn * b)) / (pn * pn);
	};
	double res;
	sum(An, res, eps);
	return 2 * res / a;
#else
	auto phi_1 = params.phi_1;
	int n = params.n;

	auto to_integrate = [a, b, eps, phi_1, x, y, n](double ksi) {
		return phi_1(ksi) * G_diff_eta(a, b, eps, x, y, ksi, 0);
	};
	double res;
	rectangle_integrate(0, a, to_integrate, n, res);
	return res;
#endif
}

double diff_x_Int_phi_1(const task_params &params, double y) {
	double a = params.a;
	double b = params.b;
	double eps = params.eps;

#ifndef APPROX
	auto An = [y, b, a](uint64_t n) {
		double pn = M_PI * n / a;
		return pn * (sin(pn * a) - pn * a * cos(pn * a)) *
			exp(-pn * y) * (1 - exp(-2 * pn * (b - y))) / (1 - exp(-2 * pn * b)) / (pn * pn);
	};
	double res;
	sum(An, res, eps);
	return 2 * res / a;
#else
	auto phi_1 = params.phi_1;
	int n = params.n;

	auto to_integrate = [phi_1, y, b, a, eps](double ksi) {
		return phi_1(ksi) * diff_x_G_diff_eta(a, b, eps, 0, y, ksi, 0);
	};
	double res;
	rectangle_integrate(0, a, to_integrate, n, res);
	return res;
#endif
}

double Int_phi_2(const task_params &params, double x, double y) {
	double a = params.a;
	double b = params.b;
	double eps = params.eps;

#ifndef APPROX
	return 0;
#else
	auto phi_2 = params.phi_2;
	int n = params.n;

	auto to_integrate = [a, b, eps, phi_2, x, y](double ksi) {
		return phi_2(ksi) * G_diff_eta(a, b, eps, x, y, ksi, b);
	};
	double res;
	rectangle_integrate(0, a, to_integrate, n, res);
	return res;
#endif
}

double diff_x_Int_phi_2(const task_params &params, double y) {
	double a = params.a;
	double b = params.b;
	double eps = params.eps;

#ifndef APPROX
	return 0;
#else
	auto phi_2 = params.phi_2;
	int n = params.n;

	auto to_integrate = [phi_2, y, b, a, eps](double ksi) {
		return phi_2(ksi) * diff_x_G_diff_eta(a, b, eps, 0, y, ksi, b);
	};
	double res;
	rectangle_integrate(0, a, to_integrate, n, res);
	return res;
#endif
}

double Int_psi_1(const task_params &params, double x, double y) {
	double a = params.a;
	double b = params.b;
	double eps = params.eps;

#ifndef APPROX
	auto An = [x, y, b, a](uint64_t n) {
		double qm = M_PI * n / b;
		return (n == 1) ? exp(-qm * x) * (1 - exp(-2 * qm * (a - x))) * sin(qm * y)
			/ 2 / (1 - exp(-2 * qm * a)) : 0;
	};
	double res;
	sum(An, res, eps);
	return 2 * res / b;
#else
	auto psi_1 = params.psi_1;
	int n = params.n;

	auto to_integrate = [a, b, eps, psi_1, x, y](double eta) {
		return psi_1(eta) * G_diff_ksi(a, b, eps, x, y, 0, eta);
	};
	double res;
	rectangle_integrate(0, b, to_integrate, n*n, res);
	return res;
#endif
}

double diff_x_Int_psi_1(const task_params &params, double y) {
	double a = params.a;
	double b = params.b;
	double eps = params.eps;

#ifndef APPROX
	auto An = [y, b, a](uint64_t n) {
		double qm = M_PI * n / b;
		return (n == 1) ? -qm * sin(qm * y) / tanh(qm * a) / 2 : 0;
	};
	double res;
	sum(An, res, eps);
	return 2 * res / b;
#else
	auto psi_1 = params.psi_1;
	int n = params.n;

	auto to_integrate = [psi_1, y, a, b, eps](double eta) {
		return psi_1(eta) * diff_x_G_diff_ksi(a, b, eps, y, 0, 0, eta);
	};
	double res;
	rectangle_integrate(0, b, to_integrate, n, res);
	return res;
#endif
}

double Int_psi_2(const task_params &params, double x, double y) {
	double a = params.a;
	double b = params.b;
	double eps = params.eps;

#ifndef APPROX
	auto An = [x, y, b, a](uint64_t n) {
		double qm = M_PI * n / b;

		return -4 * sin(qm * y) * exp(-qm * (a - x)) * (1 - exp(-2 * qm * x)) * (
			M_PI * sin(qm) - 2 * sqrt(2) * qm
			) / (1 - exp(-2 * qm * a)) / (sqr(M_PI) - sqr(4 * qm));
	};
	double res;
	sum(An, res, eps);
	return 2 * res / b;
#else
	auto psi_2 = params.psi_2;
	int n = params.n;

	auto to_integrate = [psi_2, x, y, a, b, eps](double eta) {
		return psi_2(eta) * G_diff_ksi(a, b, eps, x, y, a, eta);
	};
	double res;
	rectangle_integrate(0, b, to_integrate, n, res);
	return res;
#endif
}

double diff_x_Int_psi_2(const task_params &params, double y) {
	double a = params.a;
	double b = params.b;
	double eps = params.eps;

#ifndef APPROX
	auto An = [y, b, a](uint64_t n) {
		double qm = M_PI * n / b;
		return -4 * qm * sin(qm * y) * (M_PI * sin(qm) - 2 * sqrt(2) * qm)
			/ (sqr(M_PI) - sqr(4 * qm)) / sinh(qm * a);
	};
	double res;
	sum(An, res, eps);
	return 2 * res / b;
#else
	auto psi_2 = params.psi_2;
	int n = params.n;

	auto to_integrate = [params, psi_2, y, b, a, eps](double eta) {
		return psi_2(eta) * K(params, y, eta);
	};
	double res;
	rectangle_integrate(0, b, to_integrate, n, res);
	return res;
#endif
}

double Int_f(const task_params &params, double x, double y) {
	double a = params.a;
	double b = params.b;
	double eps = params.eps;
	double border_eps_y = params.border_eps_y;

#ifndef APPROX
	auto An = [x, y, b, a, border_eps_y](uint64_t n) {
		double pn = M_PI * n / a;
		double part1 =
			0.5 * exp(-pn * border_eps_y) * (1 - exp(-2 * pn * (b - y))) * (
				pn * (y - border_eps_y) * (1 + exp(-2 * pn * (y - border_eps_y)))
				- (1 - exp(-2 * pn * (y - border_eps_y)))
				) / (1 - exp(-2 * pn * b)) / (pn * pn);
		double part2 =
			0.5 * exp(-pn * border_eps_y) * (1 - exp(-2 * pn * y)) * (
			(1 - exp(-2 * pn * (b - y - border_eps_y)))
				+ pn * (y + border_eps_y) * (1 + exp(-2 * pn * (b - y - border_eps_y)))
				) / (1 - exp(-2 * pn * b)) / (pn * pn)
			- pn * b * exp(-pn * (b - y)) * (1 - exp(-2 * pn * y))
			/ (1 - exp(-2 * pn * b)) / (pn * pn);

		return (sin(pn * a) - pn * a * cos(pn * a)) * sin(pn * x) * (part1 + part2) / (pn * pn * pn);
	};
	double res;
	sum(An, res, eps);
	return 2 * res / a;
#else
	auto f = params.f;
	int n = params.n;

	auto external_integral = [f, x, y, a, b, eps, n](double ksi) {
		double res;
		auto internal_integral = [f, x, y, a, b, eps, ksi](double eta) {
			return f(ksi, eta) * G(a, b, eps, x, y, ksi, eta);
		};
		rectangle_integrate(0, b, internal_integral, 2 * n, res);
		return res;
	};
	double res;
	rectangle_integrate(0, a, external_integral, 2 * n, res);
	return res;
#endif
}


double diff_x_Int_f(const task_params &params, double y) {
	double a = params.a;
	double b = params.b;
	double eps = params.eps;
	double border_eps_y = params.border_eps_y;

#ifndef APPROX
	auto An = [y, b, a, border_eps_y](uint64_t n) {
		double pn = M_PI * n / a;
		double part1 =
			0.5 * exp(-pn * border_eps_y) * (1 - exp(-2 * pn * (b - y))) * (
				pn * (y - border_eps_y) * (1 + exp(-2 * pn * (y - border_eps_y)))
				- (1 - exp(-2 * pn * (y - border_eps_y)))
				) / (1 - exp(-2 * pn * b)) / (pn * pn);
		double part2 =
			0.5 * exp(-pn * border_eps_y) * (1 - exp(-2 * pn * y)) * (
			(1 - exp(-2 * pn * (b - y - border_eps_y)))
				+ pn * (y + border_eps_y) * (1 + exp(-2 * pn * (b - y - border_eps_y)))
				) / (1 - exp(-2 * pn * b)) / (pn * pn)
			- pn * b * exp(-pn * (b - y)) * (1 - exp(-2 * pn * y))
			/ (1 - exp(-2 * pn * b)) / (pn * pn);

		return (sin(pn * a) - pn * a * cos(pn * a)) * pn * (part1 + part2) / (pn * pn * pn);
	};
	double res;
	sum(An, res, eps);
	return 2 * res / a;
#else
	auto f = params.f;
	int n = params.n;

	auto external_integral = [f, y, a, b, eps, n](double ksi) {
		double res;
		auto internal_integral = [f, y, a, b, eps, ksi](double eta) {
			return f(ksi, eta) * G_diff_x(a, b, eps, 0, y, ksi, eta);
		};
		rectangle_integrate(0, b, internal_integral, 2 * n, res);
		return res;
	};
	double res;
	rectangle_integrate(0, a, external_integral, 2 * n, res);
	return res;
#endif
}

double u(const task_params &params, double x, double y) {
	return Int_phi_1(params, x, y) - Int_phi_2(params, x, y)
		+ Int_psi_1(params, x, y) - Int_psi_2(params, x, y) - Int_f(params, x, y);
}

double gy(const task_params &params, double y) {
	return diff_x_Int_phi_1(params, y) - diff_x_Int_phi_2(params, y)
		+ diff_x_Int_psi_1(params, y) - diff_x_Int_psi_2(params, y)
		- diff_x_Int_f(params, y);
}

double Ksym(const task_params &params, double t, double s) {
	int n = params.n;
	double ts_start = params.y_start;
	double ts_end = params.y_end;

	auto to_integrate = [t, s, params](double ksi) {
		return K(params, ksi, t) * K(params, ksi, s);
	};

	double res;
	rectangle_integrate(ts_start, ts_end, to_integrate, n + 1, res);
	return res;
}

double hy_precise(const task_params &params, double y) {
	return
		(
			-gy(params, y) + diff_x_Int_phi_1(params, y) - diff_x_Int_phi_2(params, y)
			+ diff_x_Int_psi_1(params, y) - diff_x_Int_f(params, y)
		);
}

double hy(const task_params &params, double y) {
	double stohastic_eps = params.stohastic_eps;
	double hy = params.hy;
	auto &theta = params.theta;

	return 
		(
			-gy(params, y) + diff_x_Int_phi_1(params, y) - diff_x_Int_phi_2(params, y)
			+ diff_x_Int_psi_1(params, y) - diff_x_Int_f(params, y)
		) * (1 + stohastic_eps * (2 * theta[y / hy] - 1));
}

double ft(const task_params &params, double t) {
	int n = params.n;

	auto to_integrate = [t, params](double s) {
		return K(params, s, t) * hy(params, s);
	};

	double res;
	rectangle_integrate(0, 1, to_integrate, n, res);
	return res;
}