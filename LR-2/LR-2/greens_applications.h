#ifndef GREENSAPP
#define GREENSAPP

#include <functional>
#include <random>
#include <vector>

struct task_params {
	double a;
	double b;
	double x_start;
	double y_start;
	double x_end;
	double y_end;
	double eps;
	double stohastic_eps;
	int n;

	double hx;
	double hy;
	double border_eps_y;
	std::vector<double> theta;

	std::function<double(double)> phi_1;
	std::function<double(double)> phi_2;
	std::function<double(double)> psi_1;
	std::function<double(double)> psi_2;
	std::function<double(double, double)> f;

	task_params(double a, double b, double x_start, double y_start,
		double x_end, double y_end, double eps, double stohastic_eps, int n);

private:
	std::default_random_engine generator;
	std::uniform_real_distribution<double> distribution;
};

double Int_phi_1(const task_params &params, double x, double y);
double Int_phi_2(const task_params &params, double x, double y);
double Int_psi_1(const task_params &params, double x, double y);
double Int_psi_2(const task_params &params, double x, double y);
double diff_x_Int_phi_1(const task_params &params, double y);
double diff_x_Int_phi_2(const task_params &params, double y);
double diff_x_Int_psi_1(const task_params &params, double y);
double diff_x_Int_psi_2(const task_params &params, double y);
double diff_x_Int_f(const task_params &params, double y);
double u(const task_params &params, double x, double y);
double gy(const task_params &params, double y);
double K(const task_params &params, double y, double eta);
double Ksym(const task_params &params, double t, double s);
double ft(const task_params &params, double t);
double hy(const task_params &params, double y);
double hy_precise(const task_params &params, double y);

#endif