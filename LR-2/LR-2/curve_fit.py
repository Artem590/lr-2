import numpy as np
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit

def func(y, b):
    return 3.458 * np.sin(b*y) + 1.


def read_data(fname):
    f = open(fname, 'r')

    x = []
    y = []

    for line in f:
        __x = float(line.split("\t")[0])
        __y = float(line.split("\t")[1])
        x.append(__x)
        y.append(__y)

    x = np.array(x)
    y = np.array(y)

    return (x, y)


X, Y = read_data('g')
X = np.insert(X, 0, np.array([0, 0, 0, 0, 0, 0, 0, 0, 0, 0]))
Y = np.insert(Y, 0, np.array([1, 1, 1, 1, 1, 1, 1, 1, 1, 1]))

X = np.insert(X, 0, np.array([1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1]))
Y = np.insert(Y, 0, np.array([0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]))

popt, pcov = curve_fit(func, X, Y)

plt.plot(np.linspace(0,1, 100), func(np.linspace(0,1, 100), *popt), 'r-', label='fit: b=%5.3f' % tuple(popt))
plt.scatter(X, Y, label='data')
plt.xlabel('x')
plt.ylabel('y')
plt.legend()
plt.show()