#ifndef UTILS
#define UTILS

#include <limits>
#include <stdint.h>

#define sqr(x) (x * x)

#define rectangle_integrate(a, b, F, n, answ) {		\
	double dX = (double)(b - a) / (n - 1);			\
	double X;										\
	answ = 0;										\
	for (int i = 0; i < n - 1; ++i) {				\
		X = a + (i * dX) + dX / 2;					\
		answ += F(X) * dX;							\
	}												\
}

#define sum(An, S, eps) {															\
	S = 0.;																			\
	double prev, next = 0;															\
	uint64_t n = 1;																	\
																					\
	do {																			\
		prev = next;																\
		next = An(n);																\
		S += next;																	\
		n++;																		\
	} while (fabs(prev - next) > eps && n < std::numeric_limits<uint64_t>::max());	\
}

#define partial_sum(An, S, n) {													\
	S = 0.;																		\
	for (uint64_t k = 1; k <= n; ++k)											\
		S += An(k);																\
}

#endif