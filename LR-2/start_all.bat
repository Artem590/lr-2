@echo off
setlocal enableextensions
SETLOCAL ENABLEDELAYEDEXPANSION

SET n=250
SET start=1.e-16
SET end=1.e-2

SET /A point_interval=n/25
SET /A point_interval_hr=n/50
SET width=330
SET height=250

for %%b in (bc without_bc) do (
    for %%p in (1 0.5 0.01) do (
        for %%e in (0 0.001 0.01 0.05 0.1) do (
            echo -n %n% -start %start% -end %end% -p %%p -eps %%e -%%b
            
            SET s=..\..\results\%%b\p_%%p\eps_%%e\
            mkdir !s!
            
            .\LR-2.exe -n %n% -start %start% -end %end% -p %%p -eps %%e -%%b > !s!output.txt
            
            SET /p aopt=<alphas.dat
            more +1 alphas.dat>al2.dat
            SET /p al2=<al2.dat
            more +2 alphas.dat>ac.dat
            SET /p ac=<ac.dat
            
            del .\*.dat
            
            gnuplot -e "set terminal png size %width%,%height%; set output '!s!delta.png'; set label 'a=!aopt!' at 0.2,0.1; plot 'L2 norm discrepancy' w lp lw 1 pointsize 1 pointinterval %point_interval% pt 3 lc 'red' title 'u by discrepancy', sin(pi*(1-x)/4) lw 2"
            gnuplot -e "set terminal png size %width%,%height%; set output '!s!L2.png'; set label 'a=!al2!' at 0.2,0.1; plot 'L2 norm' w lp lw 1 pointsize 1 pointinterval %point_interval% pt 3 lc 'red' title 'u by L2 norm', sin(pi*(1-x)/4) lw 2"
            gnuplot -e "set terminal png size %width%,%height%; set output '!s!C.png'; set label 'a=!ac!' at 0.2,0.1; plot 'C norm' w lp lw 1 pointsize 1 pointinterval %point_interval% pt 3 lc 'red' title 'u by C norm', sin(pi*(1-x)/4) lw 2"
            
            gnuplot -e "set terminal png size 600,400; set output '!s!HR_delta.png'; set label 'a=!aopt!' at 0.2,0.1; plot 'L2 norm discrepancy' w lp lw 1 pointsize 1 pointinterval %point_interval_hr% pt 3 lc 'red' title 'u by discrepancy', sin(pi*(1-x)/4) lw 2"
            gnuplot -e "set terminal png size 600,400; set output '!s!HR_L2.png'; set label 'a=!al2!' at 0.2,0.1; plot 'L2 norm' w lp lw 1 pointsize 1 pointinterval %point_interval_hr% pt 3 lc 'red' title 'u by L2 norm', sin(pi*(1-x)/4) lw 2"
            gnuplot -e "set terminal png size 600,400; set output '!s!HR_C.png'; set label 'a=!ac!' at 0.2,0.1; plot 'C norm' w lp lw 1 pointsize 1 pointinterval %point_interval_hr% pt 3 lc 'red' title 'u by C norm', sin(pi*(1-x)/4) lw 2"
        )
    )
)

